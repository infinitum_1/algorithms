#include <iostream>
#include <string>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include<sstream>
using namespace std;

class LinkedList{
    struct linked_element
    {
        linked_element* prev_el;
        int value;
        linked_element* next_el;
    };
    public: int size;
    
    private: linked_element* first;
    private: linked_element* last;
    
    public: LinkedList(){
        size=0;
        first=NULL;
        last=NULL;
       
    }
    public: void add(int value){
        if(first==NULL){
            init_list(value);
            
        }else{
            linked_element* prev= last;
            linked_element* element=new linked_element();        

            element->value=value;
            element->prev_el=prev;
            element->next_el=NULL;

            prev->next_el=element;
            last=element;

        }    
        size++;
   
    }
    
    public: void addToStart(int value){
        if(first==NULL){
            init_list(value);
            
        }else{
            linked_element* tmp = first;
            linked_element* element=new linked_element();        

            element->value=value;
            element->next_el=tmp;
            element->prev_el=NULL;

            tmp->prev_el=element;
            first=element;

        }    
        size++;
    }
    public: void deleteItemById(int id){
        linked_element* el;
        if(id != size-1){
            el=get_el(id);

        }else{
            el=last;

        }
        linked_element* prev_el=el->prev_el;
        linked_element* next_el=el->next_el;              
        
        if (prev_el!=NULL)
        {
            prev_el->next_el=next_el;
        }else
        {
            first=next_el;
        }
        if (next_el!=NULL)
        {
            next_el->prev_el=prev_el;
        }else
        {
            last=prev_el;
        }
        free(el);
        size--;        
    }    
    private: linked_element* get_el(int id){
        linked_element* el=first;
        int cur_id=0;
        while (el->next_el!=NULL && cur_id<id )
        {
            el=(*el).next_el;
            cur_id++;            
        }
        return el;
    }
    public: int get(int id){
        linked_element* el=first;
        int cur_id=0;
        while (el->next_el!=NULL && cur_id<id )
        {
            el=(*el).next_el;
            cur_id++;            
        }
        return el->value;
    }
    private: void init_list(int value){
        first = new linked_element();
        first->next_el=NULL;
        first->value=value;
        first->prev_el=NULL;
        last=first;
    }
    
    public: void show()
    
    {   
        cout<<"LIST: ";
        linked_element* next=first;
        while (next!=NULL)
        {
            cout<<next->value<<" ";
            next=next->next_el;
        }
        cout<<endl;
        
    }

};
bool check_digit(string data){
    for(int i=0; i<data.size(); i++){
        if (!isdigit(data[i]))
        {
            return false;
        }
        
    }
    return true;
}
ifstream get_file(string file_name){
     ifstream in("/home/danya/Development/algorithms/list/"+file_name);
    string path="/home/danya/Development/algorithms/list/";
    in.open(path+file_name);
    if (in)
    {
        return in;
    }
    in.open(path+"1/"+file_name);
    if (in)
    {
        return in;
    }
    in.open(path+"2/"+file_name);
 if (in)
    {
        return in;
    }
    
    
}
int main(){
    cout<<"\t COMMANDS\n";
    cout<<"add- to add number. Ex: add12\n";
    cout<<"startadd- to add number in start. Ex: startadd12\n";
    cout<<"del- to del number. Ex: del12\n";
    cout<<"get- to get element. Ex: get0\n";
    cout<<"show- to show list\n";

    cout<<"exit- to exit\n";


    LinkedList user_list;
    while (true)
    {
        cout<<endl;
        string input;
        cin>>input;
        if (input.find("startadd")!=string::npos)
        {  

            string data= input.substr(8,input.size()-1);
            if (check_digit(data))
            {
                user_list.addToStart(stoi(data));
                cout<<"element is add"<<endl;
            }else
            {
                cout<<"Input exception \n";
            }
            continue;
                        
        }
        if (input.find("add")!=string::npos)
        {  

            string data= input.substr(3,input.size()-1);
            if (check_digit(data))
            {
                user_list.add(stoi(data));
                cout<<"element is add"<<endl;
            }else
            {
                cout<<"Input exception \n";
            }
                        
        }
        
        if (input.find("exit")!=string::npos)
        {
            break;
            
        }
        if (input.find("show")!=string::npos)
        {

            user_list.show();
        }
        if (input.find("read:")!=string::npos)
        {
            string data= input.substr(5,input.size()-1);

            ifstream in=get_file(data);

            int t;
            string str;
            while (getline(in,str))
            {   
                istringstream iss(str);
                iss>>t;
                user_list.add(t);
            }
            
            user_list.show();
        }
        if (input.find("del")!=string::npos)
        {
            string data= input.substr(3,input.size()-1);
            if(check_digit(data)){
                int index= stoi(data);
                if(index>=0 && index < user_list.size){
                    user_list.deleteItemById(index);
                }else
                {
                    cout<<"Index not found \n";
                }
                
            }else
            {
                cout<<"Input exception \n";

            }
            
            cout<<endl;            
        }
        if (input.find("get")!=string::npos)
        {
            string data= input.substr(3,input.size()-1);
            if(check_digit(data)){
                int index= stoi(data);
                if(index>=0 && index < user_list.size){
                    cout<<"element: "<<user_list.get(index);

                }else
                {
                    cout<<"Index not found \n";
                }
                
            }else
            {
                cout<<"Input exception \n";
            }
            
            cout<<endl;            
        }
        
        
        
        
    }
    


}