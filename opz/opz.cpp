#include <iostream>
#include <string>
#include <stack>
#include <cstdlib>
#include <math.h>
#include <cstring>
#define BASE 16
using namespace std;

int opr_prior(char value);
string convert_to_opz(string input);
void count(string opz);
bool validate_string(string input_data);

bool is_digit(char c);//Перевірка на число
int val(char c);
int toDeci(string str, int base);//Перетворення в десяткову систему
char reVal(int num);
void strev(string* str);
string fromDeci(int base, int inputNum);//Перетворення з десяткової системи

int main()
{
    string input;

    while (true)
    {
        cout << "______________________________________________" << endl;
        input = "";
        cout << "Введіть вираз: ";
        cin >> input;

        if (input == "exit")
        {
            break;
        }

        if (validate_string(input))
        {
            string opz = convert_to_opz(input);
            count(opz);
        }
        else
        {
            cout << "Некорректний вираз"<<endl;
        }
        cout << "______________________________________________" << endl;
    }
}

string convert_to_opz(string input)
{
    string out = "";
    string out_human="";
    string cur_num = "";
    stack<char> opr;
    for (int i = 0; i < input.size(); i++)
    {
        char value = input[i];
        if (is_digit(value))
        {
            cur_num += value;

            continue;
        }
        if (out == "")
        {
            out_human=cur_num;
            cur_num=to_string(toDeci(cur_num.c_str(), BASE));
            out = cur_num;
        }
        else
        {
            if (cur_num != "")
            {
                out_human = out_human + ' ' +      cur_num;      

                cur_num=to_string(toDeci(cur_num.c_str(), BASE));
                out = out + ' ' +      cur_num;      

            }
        }
        cur_num.clear();
        if (opr.empty())
        {
            opr.push(value);
            continue;
        }
        if (value == '(')
        {
            opr.push(value);
            continue;
        }
        if (value == ')')
        {
            char cur_symb = opr.top();
            while (cur_symb != '(')
            {
                out_human=out_human+' '+cur_symb;
                out = out + ' ' + cur_symb;
                opr.pop();
                cur_symb = opr.top();
            }
            opr.pop();
            continue;
        }

        char top_value = opr.top();
        if (opr_prior(top_value) < opr_prior(value))
        {
            opr.push(value);
        }
        else
        {
            int v_prior = opr_prior(value);
            while (opr_prior(top_value) >= v_prior && (!opr.empty()))
            {
                out_human=out_human+' '+top_value;
                out = out + ' ' + top_value;
                opr.pop();
                if (!opr.empty())
                {
                    top_value = opr.top();
                }
            }
            opr.push(value);
        }
    }
    if (cur_num != "")
    {
     out_human=out_human+' '+cur_num;

      cur_num=to_string(toDeci(cur_num.c_str(), BASE));
        out = out + ' ' + cur_num;
    }

    while (!opr.empty())
    {
        char v = opr.top();
        out_human=out_human+' '+v;
        out = out + ' ' + v;
        opr.pop();
    }
                cout << "Вираз в ОПЗ: " << out_human << "\n";

    return out;
}
void count(string opz)
{
    cout << "\t Покрокове рішення\n";

    stack<int> numbers;
    string cur_num = "";
    int num_opr = 0;
    string answer="";
    for (int i = 0; i < opz.size(); i++)
    {
        char value = opz[i];
        if (is_digit(value))
        {
            cur_num += value;
        }
        else
        {
            if (value == ' ')
            {
                if (cur_num != "")
                {
                    numbers.push(stoi(cur_num));
                    cur_num.clear();
                }
                continue;
            }
            num_opr++;
            int num2 = numbers.top();
            numbers.pop();

            int num1 = numbers.top();
            numbers.pop();
            switch (value)
            {
            case '+':
                numbers.push(num1 + num2);
                break;

            case '-':
                numbers.push(num1 - num2);
                break;
            case '*':
                numbers.push(num1 * num2);
                break;
            case '/':
                numbers.push(num1 / num2);
                break;
            case '^':
                numbers.push(pow(num1, num2));
                break;
            }

            string num1_16, num2_16;
            if (num1<0)
            {
                num1_16="-";
                num1_16=num1_16+fromDeci(BASE,abs(num1));
                
            }else
            {
                num1_16=fromDeci(BASE,num1);
            }
            if (num2<0)
            {
                num2_16="-";
                num2_16=num2_16+fromDeci(BASE,abs(num2));
                
            }else
            {
                num2_16=fromDeci(BASE,num2);

            }
            answer="";
             if (numbers.top()<0)
            {
                answer="-";
                answer=answer+fromDeci(BASE,abs(numbers.top()));
                
            }else
            {
                answer=fromDeci(BASE,numbers.top());

            }
            
            
            cout << num_opr << ") " << num1_16 << value << num2_16 << '=' << answer << endl;
        }
    }
    cout << "Ответ: " <<answer  << endl;
}

bool validate_string(string input_data)
{
    int num_s_open=0;
    int num_s_close=0;
    for (int i = 0; i < input_data.size(); i++)
    {
        if (input_data[i]=='(')
        {
            num_s_open++;
        }
        if (input_data[i]==')')
        {
            num_s_close++;
        }
        
        if (is_digit(input_data[i]))
        {
            continue;
        }
        else
        {
            switch (input_data[i])
            {
            case '(':
                continue;
                break;
            case ')':
                continue;
                break;
            case '+':
                continue;
                break;
            case '-':
                continue;
                break;
            case '*':
                continue;
                break;

            case '/':
                continue;
                break;
            case '^':
                continue;
                break;
            case 'A':
                continue;
                break;
            case 'B':
                continue;
                break;
            case 'C':
                continue;
                break;
            case 'D':
                continue;
                break;
            case 'E':
                continue;
                break;
                case 'F':
                continue;
                break;
            default:
                return false;
                break;
            }
        }
    }

    if (!is_digit(input_data[input_data.size()-1]))
    {
        if (input_data[input_data.size()-1]!=')')
        {
            cout<<input_data[input_data.size()]<<endl;
            return false;
        }
        
    }
    if (num_s_close!=num_s_open)
    {
        return false;
    }
    

    
    return true;
}
int opr_prior(char value)
{
    switch (value)
    {
    case '(':
        return 0;
        break;
    case ')':
        return 1;
        break;
    case '+':
        return 2;
        break;
    case '-':
        return 2;
        break;
    case '*':
        return 3;
        break;

    case '/':
        return 3;
        break;
    case '^':
        return 4;
        break;

    default:
        break;
    }
    return 0;
}

bool is_digit(char c){
    if (isdigit(c))
        return true;

    switch (c)
    {
    case 'A':
       return true;
        break;
    case 'B':
       return true;
        break;
    case 'C':
       return true;
        break;
    case 'D':
       return true;
        break;
    case 'E':
       return true;
        break; 
      case 'F':
       return true;
        break;                   
    
    default:
    return false;
        break;
    }

    
}

int val(char c) 
{ 
    if (c >= '0' && c <= '9') 
        return (int)c - '0'; 
    else
        return (int)c - 'A' + 10; 
} 
  

int toDeci(string str, int base) 
{ 
    int len = str.length(); 
    int power = 1; 
    int num = 0; 
    int i; 
  

    for (i = len - 1; i >= 0; i--) 
    { 
       
        if (val(str[i]) >= base) 
        { 
           return -1; 
        } 
  
        num += val(str[i]) * power; 
        power = power * base; 
    } 
  
    return num; 
} 

char reVal(int num) 
{ 
    if (num >= 0 && num <= 9) 
        return (char)(num + '0'); 
    else
        return (char)(num - 10 + 'A'); 
} 
  
void strev(string* str) 
{ 
    int len = (*str).size(); 
    int i; 
    for (i = 0; i < len/2; i++) 
    { 
        char temp = (*str)[i]; 
        (*str)[i] = (*str)[len-i-1]; 
        (*str)[len-i-1] = temp; 
    } 
} 
  


string fromDeci(int base, int inputNum) 
{ 
    string res="";
    int index = 0;  
    while (inputNum > 0) 
    { 
        res.push_back(reVal(inputNum % base)); 
        inputNum /= base; 
    } 
    strev(&res); 
   
    return res;
}