#include <iostream>
using namespace std;

class Stack{
    struct linked_element
    {
        linked_element* prev_el;
        int value;
    };
    public: int size;
    
    private: linked_element* last;
    
    public: Stack(){
        size=0;
        last=NULL;
       
    }
    public: void push(int value){
        if(last==NULL){
            init_list(value);
            
        }else{
            linked_element* prev= last;
            linked_element* element=new linked_element();        

            element->value=value;
            element->prev_el=prev;

            last=element;

        }    
        size++;
   
    }
    public: int top(){
        return last->value;
    }
    public: void pop(){
        
        linked_element* el=last;
        linked_element* prev_el=el->prev_el;
        last=prev_el;
        free(el);
        size--;     
    }
    private: void init_list(int value){
        last = new linked_element();
        last->value=value;
        last->prev_el=NULL;
    }
    
    public: void show()
    
    {
        linked_element* next=last;
        cout<<"STACK: ";
        while (next!=NULL)
        {
            cout<<next->value<<" ";
            next=next->prev_el;
        }
        cout<<endl;
        
    }

};


int main(){
    Stack stack;
    cout<<"\t PUSH TEST \n";
    stack.push(90);
    stack.push(12);
    stack.push(9);
    stack.push(78);
    stack.push(7);
    stack.push(12);
    stack.show();
    cout<<"SIZE: "<<stack.size<<endl<<endl;   
    
    cout<<"\t POP TEST \n";
    int a = stack.top();
    stack.pop();
    int b = stack.top();
    
    cout<<"a: "<<a<<endl;
    cout<<"b: "<<b<<endl;
    cout<<"SIZE: "<<stack.size<<endl;
    stack.show();

}
