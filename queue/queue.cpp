#include <iostream>
using namespace std;

class Queue{
    struct linked_element
    {
        int value;
        linked_element* next_el;
    };
    public: int size;
    
    private: linked_element* first;
    private: linked_element* last;
    
    public: Queue(){
        size=0;
        first=NULL;
        last=NULL;
       
    }
    public: void enqueue(int value){
        if(first==NULL){
            init_list(value);
            
        }else{
            linked_element* prev= last;
            linked_element* element=new linked_element();        

            element->value=value;
            element->next_el=NULL;

            prev->next_el=element;
            last=element;

        }    
        size++;
   
    }
    public: int top(){
        return first->value;
    }
    public: void dequeue(){
        linked_element* el=first;
        linked_element* next_el=el->next_el;
        first=next_el;
        free(el);
        size--;   
    }    
    
    private: void init_list(int value){
        first = new linked_element();
        first->next_el=NULL;
        first->value=value;
        last=first;
    }
    
    public: void show()    
    {   
        cout<<"QUEUE: ";
        linked_element* next=first;
        while (next!=NULL)
        {
            cout<<next->value<<" ";
            next=next->next_el;
        }
        cout<<endl;
        
    }

};

int main(){
    Queue queue;
    cout<<"\t ENQUEUE TEST \n";
    queue.enqueue(90);
    queue.enqueue(12);
    queue.enqueue(2);
    queue.enqueue(45);
    queue.enqueue(9);
    queue.enqueue(89);
    queue.show();
    cout<<"SIZE: "<<queue.size<<endl<<endl;   
    
    cout<<"\t DEQUEUE TEST \n";
    int a = queue.top();
    queue.dequeue();
    int b = queue.top();
    cout<<"a: "<<a<<endl;
    cout<<"b: "<<b<<endl;
    cout<<"SIZE: "<<queue.size<<endl;
    queue.show();

}
